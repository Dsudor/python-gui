import tkinter as tk

class BluetoothGui:
    def __init__(self, master, characteristic_names):
        self.master = master
        master.title("Bluetooth Data Display")

        self.characteristic_names = characteristic_names
        self.characteristic_values = [0] * len(characteristic_names)
        self.characteristic_labels = {}

        for idx, char_name in enumerate(characteristic_names):
            label = tk.Label(master, text=f"{char_name}:")
            label.grid(row=idx, column=0, padx=10, pady=5, sticky=tk.W)

            value_label = tk.Label(master, text="0")
            value_label.grid(row=idx, column=1, padx=5, pady=5, sticky=tk.W)

            self.characteristic_labels[char_name] = value_label

        # Button to increase the value of the first characteristic
        inc_button = tk.Button(master, text="Increase First Characteristic", command=self.increment_first_characteristic)
        inc_button.grid(row=len(characteristic_names), column=0, columnspan=2, pady=10)

        # Center the interface
        self.center_window()

        # Configure row and column weights for resizing
        self.configure_resizing()

    def increment_first_characteristic(self):
        # Increment the value of the first characteristic
        if self.characteristic_values:
            self.characteristic_values[0] += 1
            self.update_characteristic_values()

    def update_characteristic_values(self):
        for char_name, value_label in self.characteristic_labels.items():
            value_label.config(text=str(self.characteristic_values[self.characteristic_names.index(char_name)]))

    def center_window(self):
        self.master.update_idletasks()
        width = self.master.winfo_width()
        height = self.master.winfo_height()
        x = (self.master.winfo_screenwidth() - width) // 2
        y = (self.master.winfo_screenheight() - height) // 2
        self.master.geometry(f"{width}x{height}+{x}+{y}")

    def configure_resizing(self):
        for i in range(len(self.characteristic_names)):
            self.master.grid_rowconfigure(i, weight=1)

        self.master.grid_columnconfigure(0, weight=1)
        self.master.grid_columnconfigure(1, weight=1)

        # Prevent automatic resizing based on contents
        self.master.pack_propagate(0)

#just an idea to change values in label by providing a handler to the notifications

if __name__ == "__main__":
    root = tk.Tk()
    characteristic_names = ["Characteristic 1", "Characteristic 2", "Characteristic 3", "Characteristic 4", "Characteristic 5"]
    bluetooth_gui = BluetoothGui(root, characteristic_names)
    root.mainloop()

import tkinter as tk

class CounterApp:
    def __init__(self, master):
        self.master = master
        master.title("Styled Counter App")

        # Dictionary to store counters
        self.counters = {}

        # Create and place labels, buttons, and entry for each counter
        for i in range(1, 6):  # You can change the number of initial counters if needed
            counter_name = f"Counter {i}"
            self.create_counter_widgets(counter_name)

        # Total sum label
        total_label = tk.Label(master, text="Total Sum:", font=("Helvetica", 12, "bold"), fg="blue")
        total_label.grid(row=len(self.counters) + 1, column=0, padx=10, pady=5, sticky=tk.W)

        self.total_sum_label = tk.Label(master, text="0", font=("Helvetica", 12), fg="green")
        self.total_sum_label.grid(row=len(self.counters) + 1, column=1, padx=5, pady=5, sticky=tk.W)

        # Reset all counters button
        reset_all_button = tk.Button(master, text="Reset All", command=self.reset_all_counters, bg="red", fg="white")
        reset_all_button.grid(row=len(self.counters) + 2, column=0, columnspan=2, pady=10)

        # Center the interface
        master.update_idletasks()
        width = master.winfo_width()
        height = master.winfo_height()
        x = (master.winfo_screenwidth() - width) // 2
        y = (master.winfo_screenheight() - height) // 2
        master.geometry(f"{width}x{height}+{x}+{y}")

        # Configure row and column weights
        for i in range(len(self.counters) + 3):  # +3 for counters, total label, and reset button
            master.grid_rowconfigure(i, weight=1)

        for j in range(5):  # Assuming there are 5 columns
            master.grid_columnconfigure(j, weight=1)

    def create_counter_widgets(self, counter_name):
        label = tk.Label(self.master, text=counter_name, font=("Arial", 10))
        label.grid(row=len(self.counters) + 1, column=0, padx=10, pady=5, sticky=tk.W)

        increment_button = tk.Button(self.master, text="Increment", command=lambda name=counter_name: self.increment_counter(name), bg="green", fg="white")
        increment_button.grid(row=len(self.counters) + 1, column=1, padx=5, pady=5)

        decrement_button = tk.Button(self.master, text="Decrement", command=lambda name=counter_name: self.decrement_counter(name), bg="orange", fg="black")
        decrement_button.grid(row=len(self.counters) + 1, column=2, padx=5, pady=5)

        reset_button = tk.Button(self.master, text="Reset", command=lambda name=counter_name: self.reset_counter(name), bg="blue", fg="white")
        reset_button.grid(row=len(self.counters) + 1, column=3, padx=5, pady=5)

        value_label = tk.Label(self.master, text="0", font=("Arial", 10))
        value_label.grid(row=len(self.counters) + 1, column=4, padx=5, pady=5)

        # Store references to labels for updating
        self.counters[counter_name] = {"label": value_label, "value": 0}

    def increment_counter(self, counter_name):
        # Increment the counter value and update the label
        self.counters[counter_name]["value"] += 1
        self.update_counters()

    def decrement_counter(self, counter_name):
        # Decrement the counter value and update the lab`el
        self.counters[counter_name]["value"] -= 1
        self.update_counters()

    def reset_counter(self, counter_name):
        # Reset the counter value to zero and update the label
        self.counters[counter_name]["value"] = 0
        self.update_counters()

    def reset_all_counters(self):
        # Reset all counters to zero and update the labels
        for counter_name in self.counters:
            self.counters[counter_name]["value"] = 0
        self.update_counters()

    def update_counters(self):
        # Update counter labels and total sum label
        total_sum = sum(counter["value"] for counter in self.counters.values())
        self.total_sum_label.config(text=str(total_sum))

        for counter_name, data in self.counters.items():
            data["label"].config(text=str(data["value"]))

if __name__ == "__main__":
    root = tk.Tk()
    app = CounterApp(root)
    root.mainloop()
